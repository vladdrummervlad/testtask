package com.vladdrummer.testtask;

import android.animation.Animator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */
public class FabScrollBehavior extends CoordinatorLayout.Behavior<FloatingActionButton>  {

    boolean isAnimating = false;
    public FabScrollBehavior(Context context, AttributeSet attrs) {
        super();
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout,
                                       @NonNull FloatingActionButton child,
                                       @NonNull View directTargetChild, @NonNull View target,
                                       int axes, int type) {
        return true;
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout,
                               @NonNull final FloatingActionButton child, @NonNull View target,
                               int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed,
                               int type) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed,
                dxUnconsumed, dyUnconsumed, type);
        if (dyConsumed > 50 && child.getVisibility() == View.VISIBLE) {
            hide(child);
        } else if (dyConsumed <= -50 && child.getVisibility() != View.VISIBLE) {
            show(child);
        }
    }


    private void hide(final FloatingActionButton floatingActionButton) {
        if (isAnimating) {
            return;
        }
        isAnimating = true;
        int bottomMargin = ((ViewGroup.MarginLayoutParams) floatingActionButton.getLayoutParams())
                .bottomMargin;
        floatingActionButton.animate().translationY(floatingActionButton.getHeight() + bottomMargin)
                .setInterpolator(new AccelerateInterpolator(2)).setListener(
                        new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                floatingActionButton.setVisibility(View.INVISIBLE);
                isAnimating = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
    }

    private void show( FloatingActionButton floatingActionButton ) {
        if (isAnimating) {
            return;
        }
        isAnimating = true;
        floatingActionButton.animate().translationY(0)
                .setInterpolator(new DecelerateInterpolator(2)).setListener(
                        new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimating = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
        floatingActionButton.setVisibility(View.VISIBLE);

    }
}