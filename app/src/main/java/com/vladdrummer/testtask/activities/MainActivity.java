package com.vladdrummer.testtask.activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import com.vladdrummer.testtask.R;
import com.vladdrummer.testtask.fragments.DetailFragment;
import com.vladdrummer.testtask.fragments.FeedFragment;
import com.vladdrummer.testtask.models.Post;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FeedFragment fragment =  new FeedFragment();
        showFragment(fragment, R.id.content_view);

        fragment.setOnFeedFragmentListener(listener);

        setSupportActionBar(toolbar);

    }


    private FeedFragment.OnFeedFragmentListener listener
            = new FeedFragment.OnFeedFragmentListener() {
        @Override
        public void onDetails(Post post) {
           showDetailFragment(post);
        }

        @Override
        public void onSearch(String value) {
            showSearchFragment(value);
        }
    };

    private void showDetailFragment(Post post) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(DetailFragment.POST_ARGUMENT, post);
        detailFragment.setArguments(arguments);
        showFragment(detailFragment, R.id.content_view);
    }

    private void showSearchFragment(String value) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        FeedFragment feedFragment = new FeedFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(FeedFragment.SEARCH_ARGUMENT, value);
        feedFragment.setArguments(arguments);
        feedFragment.setOnFeedFragmentListener(listener);
        showFragment(feedFragment, R.id.content_view);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    private void showFragment(Fragment fragment, int containerId) {
        showFragment(fragment, containerId, R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
    }

    private void showFragment(Fragment fragment, int containerId, int animEnter, int animExit,
                              int animPopEnter, int animPopExit) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 0
                && fragmentManager.getFragments().size() == 0) {
            FragmentTransaction transaction = fragmentManager.beginTransaction()
                    .replace(containerId, fragment);
            transaction.setCustomAnimations(animEnter, animExit);
            transaction.commit();
        } else {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(animEnter, animExit,
                    animPopEnter, animPopExit);
            transaction.replace(containerId, fragment).addToBackStack(null);
            transaction.commit();
        }
    }


}
