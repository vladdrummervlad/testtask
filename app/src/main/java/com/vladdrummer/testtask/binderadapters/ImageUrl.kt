package com.vladdrummer.testtask.binderadapters

import android.databinding.BindingAdapter
import android.text.TextUtils
import android.widget.ImageView
import com.vladdrummer.testtask.GlideApp
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.squareup.picasso.Picasso

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */
@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, url: String?) {
    try {
        if (TextUtils.isEmpty(url)) {
            view.setImageBitmap(null)
            return
        }
        GlideApp
                .with(view.context)
                .load(url)
                .override(400, 200)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(view)
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
    }

}