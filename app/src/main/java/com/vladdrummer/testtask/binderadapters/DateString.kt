package com.vladdrummer.testtask.binderadapters

import android.databinding.BindingAdapter
import android.text.TextUtils
import android.widget.TextView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */
@BindingAdapter("bind:date")
fun showDate(view: TextView, timestamp: String?) {
    try {
        if (TextUtils.isEmpty(timestamp)) {
            view.text = null
            return
        }
        val df = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.US)
        var result: Date
        try {
            result = df.parse(timestamp)
            var date = DateFormat.getDateTimeInstance().format(result.time)
            view.text = date

        } catch (e: Exception) {
            e.printStackTrace()
        }
    } catch (e: IllegalArgumentException) {
        e.printStackTrace()
    }

}