package com.vladdrummer.testtask.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */
public class ResultList<T> {

    @SerializedName("items")
    private List<T> items;

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
