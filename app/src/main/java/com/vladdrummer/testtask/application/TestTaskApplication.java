package com.vladdrummer.testtask.application;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.vladdrummer.testtask.database.TestTaskDatabase;

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */

public class TestTaskApplication extends Application {

    private static TestTaskDatabase sDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        sDatabase = Room.databaseBuilder(this, TestTaskDatabase.class, "db").build();
    }


    public static TestTaskDatabase getDatabase() {
        return sDatabase;
    }

}
