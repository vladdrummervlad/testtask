package com.vladdrummer.testtask.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;



import com.vladdrummer.testtask.models.Post;

/**
 * Created by Vlad Alexeev on 2017/12/05.
 */
@Database(entities = {Post.class}, version = 1)
@TypeConverters(Converter.class)
public abstract class TestTaskDatabase extends RoomDatabase {

    public abstract PostDao postDao();

}
