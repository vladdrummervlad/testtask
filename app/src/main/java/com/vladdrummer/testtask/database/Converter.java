package com.vladdrummer.testtask.database;

import android.arch.persistence.room.TypeConverter;

import com.vladdrummer.testtask.models.Author;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */
public class Converter {

    @TypeConverter
    public String stringFromAuthor(Author value) {
        if (value == null) {
            return null;
        }
        String json = new Gson().toJson(value);
        return json;
    }

    @TypeConverter
    public Author AuthorFromString(String value) {
        if (value == null) {
            return new Author();
        }
        Type type = new TypeToken<Author>() {}.getType();
        Author author = new Gson().fromJson(value, type);
        return author;
    }

}
