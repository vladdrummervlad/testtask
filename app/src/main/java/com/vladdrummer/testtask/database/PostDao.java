package com.vladdrummer.testtask.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.vladdrummer.testtask.models.Post;

import java.util.List;

/**
 * Created by Vlad Alexeev on 2017/12/05.
 */
@Dao
public interface PostDao {

    @Query("SELECT * FROM post")
    LiveData<List<Post>> getAll();

    @Query("SELECT * FROM post where title like :value or title like :value or content like :value")
    LiveData<List<Post>> search(String value);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Post... stories);

}
