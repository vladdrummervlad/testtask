package com.vladdrummer.testtask;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Vlad Alexeev on 2017/12/07.
 */
@GlideModule
public class TestTaskGlideModule extends AppGlideModule {
}
