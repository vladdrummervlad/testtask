package com.vladdrummer.testtask.fragments;

/**
 * Created by Vlad Alexeev on 2017/12/06.
 */

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.vladdrummer.testtask.R;
import com.vladdrummer.testtask.adapters.FeedAdapter;
import com.vladdrummer.testtask.application.TestTaskApplication;
import com.vladdrummer.testtask.databinding.FragmentMainBinding;
import com.vladdrummer.testtask.models.Post;
import com.vladdrummer.testtask.viewmodels.FeedViewModel;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class FeedFragment extends Fragment {

    public final static String SEARCH_ARGUMENT = "search";



    private FragmentMainBinding mBinding;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FloatingActionButton mFab;
    private FeedAdapter mAdapter;
    private OnFeedFragmentListener mOnFeedFragmentListener;
    private FeedViewModel mFeedModel;
    private String mSearch;

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentMainBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_main, container, false);

        mRecyclerView = binding.recyclerView;
        mSwipeRefreshLayout = binding.swiperefresh;
        mFab = binding.fab;

        boolean showBack;
        String title;


        Bundle arguments = getArguments();
        if (arguments != null) {
            mSearch = arguments.getString(SEARCH_ARGUMENT);
        }

        if (mRecyclerView == null) {
            return null;
        }
        mAdapter = new FeedAdapter(getActivity());
        mAdapter.setOnItemClickListener(mOnItemClickListener);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        LiveData<List<Post>> liveData;
        if (TextUtils.isEmpty(mSearch)) {
            mFeedModel = ViewModelProviders.of(this).get(FeedViewModel.class);
            liveData = mFeedModel.updateFeed(mErrorListener);
            title = getString(R.string.feed);
            showBack = false;
        } else {
            mSwipeRefreshLayout.setEnabled(false);
            mFab.setVisibility(View.GONE);
            liveData = TestTaskApplication.getDatabase().postDao()
                    .search('%' + mSearch + '%');
            title = getString(R.string.search_result);
            showBack = true;
        }

        liveData.observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                mAdapter.setItems(posts);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFeedModel.updateFeed(mErrorListener);
            }
        });

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchDialog();
            }
        });

        ((AppCompatActivity) getActivity()).getSupportActionBar()
                .setDisplayHomeAsUpEnabled(showBack);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        this.mBinding = binding;
        return binding.getRoot();
    }



    void showSearchDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.view_search, null);
        dialogBuilder.setView(dialogView);

        final EditText editText = (EditText) dialogView.findViewById(R.id.search_edit_text);

        dialogBuilder.setTitle(getString(R.string.search_post));
        dialogBuilder.setPositiveButton(getString(R.string.search),
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = editText.getText().toString();
                if (!TextUtils.isEmpty(value) || mOnFeedFragmentListener != null) {
                    mOnFeedFragmentListener.onSearch(value);

                }
            }
        });
        dialogBuilder.setNegativeButton(android.R.string.cancel, null);
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    FeedViewModel.OnErrorListener mErrorListener = new FeedViewModel.OnErrorListener() {
        @Override
        public void onUpdateError() {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), R.string.error_update, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private FeedAdapter.OnItemClickListener mOnItemClickListener =
            new FeedAdapter.OnItemClickListener() {
                @Override
                public void onCLick(Post post) {
                    if (mOnFeedFragmentListener != null) {
                        mOnFeedFragmentListener.onDetails(post);
                    }
                }
            };

    public void setOnFeedFragmentListener(OnFeedFragmentListener onFeedFragmentListener) {
        mOnFeedFragmentListener = onFeedFragmentListener;
    }

    public interface OnFeedFragmentListener {

        void onDetails(Post post);

        void onSearch(String value);

    }
}