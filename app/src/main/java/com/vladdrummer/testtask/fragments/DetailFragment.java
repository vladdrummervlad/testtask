package com.vladdrummer.testtask.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vladdrummer.testtask.GlideApp;
import com.vladdrummer.testtask.R;
import com.vladdrummer.testtask.adapters.FeedAdapter;
import com.vladdrummer.testtask.databinding.FragmentDetailsBinding;
import com.vladdrummer.testtask.models.Post;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by Vlad Alexeev on 2017/12/06.
 */

public class DetailFragment extends Fragment {

    public final static String POST_ARGUMENT = "post";

    private FragmentDetailsBinding mBinding;
    private RecyclerView mRecyclerView;
    private FeedAdapter mAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentDetailsBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_details, container, false);
        final Post post = (Post) getArguments().getSerializable(POST_ARGUMENT);
        if (post != null) {
            binding.setPost(post);
        }

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Details");

        TextView contentTextView = binding.contentTextView;
        contentTextView.setClickable(true);
        contentTextView.setMovementMethod (LinkMovementMethod.getInstance());

        binding.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share(post);
            }
        });

        this.mBinding = binding;
        return binding.getRoot();


    }

    private void share(final Post post) {
        GlideApp
                .with(getActivity())
                .asFile()
                .load(post.getUrl())
                .onlyRetrieveFromCache(true)
                .into(new SimpleTarget<File>() {
                    @Override
                    public void onResourceReady(File resource, Transition<? super File> transition) {
                        Uri uri = Uri.fromFile(resource);
                        try {
                            File temporaryFile = File.createTempFile("image", ".jpg", getActivity().getExternalCacheDir() );
                            copy(resource, temporaryFile);
                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                            shareIntent.setType("image/*");
                            shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, post.getTitle());
                            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(post.getContent()));
                            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(temporaryFile));
                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(shareIntent, "Share Deal"), 6);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Integer i1 = 1;
                        Integer i2 = 2;
                        i1.equals(i2);


                    }
                });



    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }


}
