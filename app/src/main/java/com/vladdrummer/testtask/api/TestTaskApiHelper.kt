package com.vladdrummer.testtask.api

import com.vladdrummer.testtask.models.Post
import com.vladdrummer.testtask.models.ResultList
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Vlad Alexeev on 2017/12/05.
 */

object TestTaskApiHelper {

    private val API_URL = "https://laravel-news.com/"
    private val retrofit: Retrofit
    private val api: TestTaskApi
    val logInterceptor = HttpLoggingInterceptor()

    init {
        val client = OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .build()
        retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        api = retrofit.create(TestTaskApi::class.java)
    }

    fun getFeed(): List<Post>? {


        val call: Call<ResultList<Post>> = api.feed()
        try {
            val response = call.execute()
            return if (response.isSuccessful) {
                response.body()?.items
            } else {
                null
            }
        } catch (e: Exception) {
            return null
        }
    }



}
