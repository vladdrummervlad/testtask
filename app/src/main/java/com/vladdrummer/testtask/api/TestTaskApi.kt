package com.vladdrummer.testtask.api

import com.vladdrummer.testtask.models.Post
import com.vladdrummer.testtask.models.ResultList

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Vlad Alexeev on 2017/12/05.
 */
interface TestTaskApi {

    @GET("feed/json")
    fun feed(): Call<ResultList<Post>>
}
